#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <limits.h>

#define ITEMS_NUMBER 33

#define RESET   "\033[0m"
#define BLUE    "\033[1m\033[34m"      /* Bold Blue */
#define RED     "\033[1m\033[31m"      /* Bold Red */

#define CONTAINER_BIT_SIZE (sizeof(container_t) * 8)
#define ITEM_BIT_SIZE      (sizeof(item_t) * 8)

#define CONTAINER_FORMAT "%lx"
#define ITEM_FORMAT      "%x"

#define ITEM_MAX_VAL     UINT8_MAX

typedef uint64_t container_t;
typedef uint8_t  item_t;

static const char *to_binary(container_t x, size_t size)
{
    if (size > CONTAINER_BIT_SIZE)
        return NULL;

    static char b[CONTAINER_BIT_SIZE + 1];
    b[size] = '\0';

    container_t index = 1;

    while (size)
    {
        b[size - 1] = (x & index) ? '1' : '0';
        --size;
        index = index << 1;
    }

    return b;
}

static container_t build_mask(item_t useful)
{
    container_t mask = 1;

    while (useful > 1)
    {
        mask = mask << 1;
        ++mask;
        --useful;
    }

    return mask;
}

static item_t calculate_blocks_number(const item_t pack_size)
{
    return (ITEMS_NUMBER + pack_size - 1) / pack_size;
}

container_t *pack(const item_t *string, const item_t useful)
{
    if ((useful == 0) || (useful > ITEM_BIT_SIZE))
        return NULL;

    const item_t   pack_size = CONTAINER_BIT_SIZE / useful;
    container_t    *res = calloc(calculate_blocks_number(pack_size),
                                 CONTAINER_BIT_SIZE / 8);
    container_t    *current_item = res;
    container_t     mask = build_mask(useful);
    item_t          i = 0;

    while (i < ITEMS_NUMBER)
    {
        *current_item = *current_item << useful;
        *current_item = *current_item | (string[i] & mask);
        ++i;
        if (!(i % pack_size))
            ++current_item;
    }

    return res;
}

static item_t unpack_calculate_shift(item_t pack_size, item_t i)
{
    item_t items_left = ITEMS_NUMBER - i;
    return (items_left < pack_size) ? items_left
                                    : pack_size;
}

item_t *unpack(const container_t *string, const item_t useful)
{
    if ((useful == 0) || (useful > ITEM_BIT_SIZE))
        return NULL;

    const item_t    pack_size = CONTAINER_BIT_SIZE / useful;
    item_t         *res = calloc(ITEMS_NUMBER, ITEM_BIT_SIZE / 8);
    container_t     current_item = *string;
    container_t     mask = build_mask(useful);
    item_t          i = 0;
    item_t          curr_index = unpack_calculate_shift(pack_size, i) - 1;

    while (i < ITEMS_NUMBER)
    {
        res[curr_index - (i % pack_size)] = (item_t)current_item & mask;
        current_item = current_item >> useful;

        ++i;
        if (!(i % pack_size))
        {
            ++string;
            current_item = *string;
            curr_index = curr_index + unpack_calculate_shift(pack_size, i);
        }
    }

    return res;
}

static container_t *test_pack(const item_t *string, const item_t useful)
{
    container_t *packed_string = pack(string, useful);
    if (!packed_string)
    {
        printf("ERROR\n");
        return NULL;
    }

    const item_t res_len = calculate_blocks_number(CONTAINER_BIT_SIZE / useful);

    for (item_t i = 0; i < res_len; ++i)
        printf(CONTAINER_FORMAT" ", packed_string[i]);
    printf("\n");

    for (item_t i = 0; i < res_len; ++i)
        printf("%s\n",  to_binary(packed_string[i], ITEM_BIT_SIZE));
    printf("\n");

    return packed_string;
}

static item_t *test_unpack(const container_t *string, const item_t useful)
{
    item_t *unpacked_string = unpack(string, useful);
    if (!unpacked_string)
    {
        printf("ERROR\n");
        return NULL;
    }

    for (int i = 0; i < ITEMS_NUMBER; ++i)
        printf(ITEM_FORMAT" ", unpacked_string[i]);
    printf("\n");

    for (item_t i = 0; i < ITEMS_NUMBER; ++i)
    {
        if (!(i % 10))
            printf("\n");
        printf("%s ",  to_binary(unpacked_string[i], ITEM_BIT_SIZE));
    }
    printf("\n");

    return unpacked_string;
}

int main (void)
{
    if (CONTAINER_BIT_SIZE < ITEM_BIT_SIZE)
    {
        printf(RED"ERROR: check typedefs\n"RESET);
        return 0;
    }

    item_t string[ITEMS_NUMBER];

    printf(RED"INITIALIZATION\n"RESET);
    for (int i = 0; i < ITEMS_NUMBER; ++i)
    {
        string[i] = rand() % ITEM_MAX_VAL;
        printf(ITEM_FORMAT" ", string[i]);
    }
    printf("\n");
    for (int i = 0; i < ITEMS_NUMBER; ++i)
    {
        if (!(i % 10))
            printf("\n");
        printf("%s ",  to_binary(string[i], sizeof(*string) * 8));
    }
    printf("\n");

    {
        printf(RED"TEST1\n"RESET);
        container_t *packed_string = NULL;
        item_t  *unpacked_string = NULL;

        printf(BLUE"Pack 8 bits: "RESET);
        packed_string = test_pack(string, 8);

        printf(BLUE"Unack 8 bits: "RESET);
        unpacked_string = test_unpack(packed_string, 8);
        free(unpacked_string);

        printf(BLUE"Unack 4 bits: "RESET);
        unpacked_string = test_unpack(packed_string, 4);
        free(unpacked_string);

        printf(BLUE"Unack 2 bits: "RESET);
        unpacked_string = test_unpack(packed_string, 2);
        free(unpacked_string);

        free(packed_string);
    }

    {
        printf(RED"TEST2\n"RESET);
        container_t *packed_string = NULL;
        item_t  *unpacked_string = NULL;

        printf(BLUE"Pack 4 bits: "RESET);
        packed_string = test_pack(string, 4);

        printf(BLUE"Unack 4 bits: "RESET);
        unpacked_string = test_unpack(packed_string, 4);
        free(unpacked_string);

        printf(BLUE"Unack 2 bits: "RESET);
        unpacked_string = test_unpack(packed_string, 2);
        free(unpacked_string);

        free(packed_string);
    }

    {
        printf(RED"TEST3\n"RESET);
        container_t *packed_string = NULL;
        item_t  *unpacked_string = NULL;

        printf(BLUE"Pack 4 bits: "RESET);
        packed_string = test_pack(string, 4);

        printf(BLUE"Unack 4 bits: "RESET);
        unpacked_string = test_unpack(packed_string, 4);
        free(packed_string);

        printf(BLUE"Pack 4 bits: "RESET);
        packed_string = test_pack(unpacked_string, 4);
        free(packed_string);
        free(unpacked_string);
    }

    {
        printf(RED"TEST4\n"RESET);
        container_t *packed_string = NULL;
        item_t  *unpacked_string = NULL;

        printf(BLUE"Pack 6 bits: "RESET);
        packed_string = test_pack(string, 6);

        printf(BLUE"Unack 6 bits: "RESET);
        unpacked_string = test_unpack(packed_string, 6);
        free(unpacked_string);
        free(packed_string);
    }

    {
        printf(RED"TEST5\n"RESET);
        container_t *packed_string = NULL;
        item_t  *unpacked_string = NULL;

        printf(BLUE"Pack 16 bits: "RESET);
        packed_string = test_pack(string, 16);

        printf(BLUE"Unack 16 bits: "RESET);
        unpacked_string = test_unpack(packed_string, 16);
        free(unpacked_string);
        free(packed_string);
    }

    {
        printf(RED"TEST7\n"RESET);
        container_t *packed_string = NULL;
        item_t  *unpacked_string = NULL;

        printf(BLUE"Pack 32 bits: "RESET);
        packed_string = test_pack(string, 32);

        printf(BLUE"Unack 32 bits: "RESET);
        unpacked_string = test_unpack(packed_string, 32);
        free(unpacked_string);
        free(packed_string);
    }

    return 0;
}

